import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:theme_xample/home.dart';
import 'package:theme_xample/theme.dart';

void main() {

  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(create: (context)=> SwatchColorNotifier(yellow))
  ], child: MyApp(),));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final colorProvider = Provider.of<SwatchColorNotifier>(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: colorProvider.getColor),
      home: Home(),
    );
  }
}
