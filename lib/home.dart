import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:theme_xample/theme.dart';

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<SwatchColorNotifier>(context);
    return Scaffold(
      appBar: AppBar(title: Text("GOOOOOOD"),),
      body: Container(child: Column(children: [
        IconButton(onPressed: (){
          provider.setColor(yellow);
        }, icon: Icon(Icons.piano)),
        IconButton(onPressed: (){
          provider.setColor(purple);
        }, icon: Icon(Icons.dangerous)),
        IconButton(onPressed: (){
          provider.setColor(orange);
        }, icon: Icon(Icons.add)),
      ],),),
    );
  }
}