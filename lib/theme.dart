import 'package:flutter/material.dart';

Map<int, Color> yell = {
  50: Color(0xffFFD83D),
  100: Color(0xffFFD83D).withOpacity(0.2),
  200: Color(0xffFFD83D).withOpacity(0.3),
  300: Color(0xffFFD83D).withOpacity(0.4),
  400: Color(0xffFFD83D).withOpacity(0.5),
  500: Color(0xffFFD83D).withOpacity(0.6),
  600: Color(0xffFFD83D).withOpacity(0.7),
  700: Color(0xffFFD83D).withOpacity(0.8),
  800: Color(0xffFFD83D).withOpacity(0.9),
  900: Color(0xffFFD83D),
};
Map<int, Color> ora = {
  50: Color(0xffFF9B8B),
  100: Color(0xffFF9B8B).withOpacity(0.2),
  200: Color(0xffFF9B8B).withOpacity(0.3),
  300: Color(0xffFF9B8B).withOpacity(0.4),
  400: Color(0xffFF9B8B).withOpacity(0.5),
  500: Color(0xffFF9B8B).withOpacity(0.6),
  600: Color(0xffFF9B8B).withOpacity(0.7),
  700: Color(0xffFF9B8B).withOpacity(0.8),
  800: Color(0xffFF9B8B).withOpacity(0.9),
  900: Color(0xffFF9B8B),
};
Map<int, Color> pur = {
  50: Color(0xffD9D3FF),
  100: Color(0xffD9D3FF).withOpacity(0.2),
  200: Color(0xffD9D3FF).withOpacity(0.3),
  300: Color(0xffD9D3FF).withOpacity(0.4),
  400: Color(0xffD9D3FF).withOpacity(0.5),
  500: Color(0xffD9D3FF).withOpacity(0.6),
  600: Color(0xffD9D3FF).withOpacity(0.7),
  700: Color(0xffD9D3FF).withOpacity(0.8),
  800: Color(0xffD9D3FF).withOpacity(0.9),
  900: Color(0xffD9D3FF),
};


MaterialColor yellow = MaterialColor(0xffFFD83D, yell);
MaterialColor orange = MaterialColor(0xffFF9B8B, ora);
MaterialColor purple = MaterialColor(0xffD9D3FF, pur);


ThemeData activeTheme = yellowTheme;
final yellowTheme = ThemeData(primarySwatch: yellow);
final orangeTheme = ThemeData(primarySwatch: orange);
final purpleTheme = ThemeData(primarySwatch: purple);


onChangeTheme(int toIntColor,ThemeNotifier provider){
  if(toIntColor==1){
    provider.setTheme(yellowTheme);
    activeTheme = yellowTheme;
  }else if(toIntColor==2){
    provider.setTheme(orangeTheme);
    activeTheme = orangeTheme;
  }else {
    provider.setTheme(purpleTheme);
    activeTheme = purpleTheme;
  }

  // SharePre
}

// Theme
class ThemeNotifier extends ChangeNotifier{
  ThemeData _themeData;
  ThemeNotifier(this._themeData);
  ThemeData get getTheme => _themeData;

  setTheme(ThemeData themeData){
    _themeData = themeData;
    notifyListeners();
  }
}

// Color
class SwatchColorNotifier extends ChangeNotifier{
  MaterialColor _color;
  SwatchColorNotifier(this._color);
  MaterialColor get getColor => _color;

  setColor(MaterialColor color){
    _color = color;
    notifyListeners();
  }
}